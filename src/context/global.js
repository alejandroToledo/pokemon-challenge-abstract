import React, { createContext, useContext, useEffect, useReducer, useState } from "react";
import axios from 'axios';
import { debounce } from "lodash";
import { useToast } from "@chakra-ui/react";

// Global context to store data related to pokemons and their management.
const GlobalContext = createContext();

// Actions
const DELETE_STARTED = "DELETE_STARTED";
const DELETE_SUCCESS = "DELETE_SUCCESS";

const GET_POKEMONS_STARTED = "GET_POKEMONS_STARTED";
const GET_POKEMONS_SUCCESS = "GET_POKEMONS_SUCCESS";

const GET_POKEMON_DATABASE_STARTED = "GET_POKEMON_DATABASE_STARTED";
const GET_POKEMON_DATABASE_SUCCESS = "GET_POKEMON_DATABASE_SUCCESS";

const PAGINATION_STARTED = "PAGINATION_STARTED";
const PAGINATION_SUCCESS = "PAGINATION_SUCCESS";

const SEARCH_STARTED = "SEARCH_STARTED";
const SEARCH_SUCCESS = "SEARCH_SUCCESS";

const GET_CATCHED_POKEMONS_STARTED = "GET_CATCHED_POKEMONS_STARTED";
const GET_CATCHED_POKEMONS_SUCCESS = "GET_CATCHED_POKEMONS_SUCCESS";

// Reducer
const reducer = (state, action) => {
    switch (action.type) {
        // GET POKEMON DATABASE ACTIONS
        case GET_POKEMON_DATABASE_STARTED:
            return { ...state, loading: true };

        case GET_POKEMON_DATABASE_SUCCESS:
            return { ...state, pokemonDataBase: action.payload, loading: false };

        // GET POKEMONS PAGINATION ACTIONS
        case GET_POKEMONS_STARTED:
            return { ...state, loading: true };

        case GET_POKEMONS_SUCCESS:
            return {
                ...state,
                pokemons: action.payload.results,
                next: action.payload.next,
                loading: false
            };

        // GET CATCHED POKEMONS ACTIONS
        case GET_CATCHED_POKEMONS_STARTED:
            return { ...state, loading: true };

        case GET_CATCHED_POKEMONS_SUCCESS:
            return {
                ...state,
                catchedPokemons: action.payload.results,
                loading: false,
            };

        // DELETE ACTIONS
        case DELETE_STARTED:
            return { ...state, loading: true };

        case DELETE_SUCCESS:
            return { ...state, loading: false };

        // PAGINATION ACTIONS
        case PAGINATION_STARTED:
            return { ...state, loadingPagination: true };

        case PAGINATION_SUCCESS:
            return { ...state, loadingPagination: false, next: action.payload.next, };

        // SEARCH ACTIONS
        case SEARCH_STARTED:
            return { ...state, loadingSearch: true };

        case SEARCH_SUCCESS:
            return { ...state, loadingSearch: false };
    }

    return state;
};

export const GlobalProvider = ({ children }) => {
    const toast = useToast();
    const url = "https://pokeapi.co/api/v2";
    const localUrl = "/api";

    // Initial state for the global context
    const initialState = {
        pokemons: [],
        catchedPokemons: [],
        pokemonDataBase: [],
        searchResults: [],
        next: "",
        loading: true,
        loadingPagination: false,
        loadingSearch: false
    };

    const [state, dispatch] = useReducer(reducer, initialState);
    const [pokemons, setPokemons] = useState([]);
    const [catchedPokemons, setCatchedPokemons] = useState([]);

    // Error handling function
    const handleError = (error) => {
        console.error('Error:', error);
        toast({
            position: 'bottom-left',
            title: "Error",
            description: "An error occurred.",
            status: "error",
            duration: 3000,
            isClosable: true,
        });
    };

    // GET ALL POKEMON DATABASE
    const getPokemonDatabase = async () => {
        try {
            dispatch({ type: GET_POKEMON_DATABASE_STARTED });

            const response = await axios.get(`${url}/pokemon?limit=1026&offset=0`);

            dispatch({ type: GET_POKEMON_DATABASE_SUCCESS, payload: response.data.results });
        } catch (error) {
            handleError(error)
        }
    };

    // GET POKEMONS WITH PAGINATION
    const getPokemons = async () => {
        try {
            dispatch({ type: GET_POKEMONS_STARTED });

            const response = await axios.get(`${url}/pokemon/?limit=20&offset=0`);

            const pokemonsData = await Promise.all(response.data.results.map(async pokemon => {
                const pokemonResponse = await axios.get(pokemon.url);
                return pokemonResponse.data;
            }));

            setPokemons(pokemonsData);

            dispatch({ type: GET_POKEMONS_SUCCESS, payload: response.data });
        } catch (error) {
            handleError(error)
        }
    };

    // GET CATCHED POKEMONS
    const getCatchedPokemons = async () => {
        try {
            dispatch({ type: GET_CATCHED_POKEMONS_STARTED });

            const response = await axios.get(`${localUrl}/catched`);

            setCatchedPokemons(response.data);

            dispatch({ type: GET_CATCHED_POKEMONS_SUCCESS, payload: response.data });
        } catch (error) {
            handleError(error)
        }
    };

    // CATCH A POKEMON
    const catchPokemon = async (pokemon) => {
        try {
            const response = await axios.post(`${localUrl}/catched`, {
                id: pokemon.id,
                name: pokemon.name,
            });

            if (response.status === 200) {
                toast({
                    position: 'bottom-left',
                    title: "Pokemon Caught",
                    description: `${pokemon.name} has been caught.`,
                    status: "success",
                    duration: 3000,
                    isClosable: true,
                });
                getCatchedPokemons();
            }
        } catch (error) {
            handleError(error)
        }
    };

    // RELEASE A POKEMON
    const releasePokemon = async (pokemonId) => {
        try {
            dispatch({ type: DELETE_STARTED });
            const response = await axios.delete(`${localUrl}/catched/${pokemonId}`);

            if (response.status === 200) {
                toast({
                    position: 'bottom-left',
                    title: "Pokemon Released",
                    description: "The Pokemon has been released successfully.",
                    status: "success",
                    duration: 3000,
                    isClosable: true,
                });
                getCatchedPokemons();
            }
            dispatch({ type: DELETE_SUCCESS });
        } catch (error) {
            handleError(error)
        }
    };

    // NEXT PAGE
    const nextPage = async () => {
        try {
            dispatch({ type: PAGINATION_STARTED });
            const response = await axios.get(state.next);

            // Fetch the new pokemon data
            const newPokemons = await Promise.all(response.data.results.map(async pokemon => {
                const pokemonRes = await axios.get(pokemon.url);
                return pokemonRes.data;
            }));

            // Add new pokemon data to the old pokemon data
            setPokemons([...pokemons, ...newPokemons]);

            dispatch({ type: PAGINATION_SUCCESS, payload: response.data });
        } catch (error) {
            console.error('Error fetching next page:', error);
            handleError(error)
        }
    };

    // SEARCH POKEMON IN DATABASE
    const realTimeSearch = debounce(async (search) => {
        dispatch({ type: SEARCH_STARTED });
        if (search !== "") {
            // Search pokemon database
            const response = state.pokemonDataBase.filter(pokemon =>
                pokemon.name.toLowerCase().includes(search.toLowerCase())
            );
            // Fetch the new pokemon data
            const pokemons = await Promise.all(response.map(async pokemon => {
                const pokemonResponse = await axios.get(pokemon.url);
                return pokemonResponse.data;
            }));
            setPokemons(pokemons);
        } else {
            getPokemons();
        }
        dispatch({ type: SEARCH_SUCCESS });
    }, 500);

    useEffect(async () => {
        await getPokemonDatabase();
        await getPokemons();
        await getCatchedPokemons();
    }, []);

    return (
        <GlobalContext.Provider
            value={{
                ...state,
                pokemons,
                catchedPokemons,
                nextPage,
                catchPokemon,
                releasePokemon,
                realTimeSearch,
            }}
        >
            {children}
        </GlobalContext.Provider>
    );
};

export const useGlobalContext = () => {
    return useContext(GlobalContext);
};
