import { useState } from "react";
import {
  Container,
  Stack,
  Input,
  InputGroup,
  Button,
  SimpleGrid,
  Flex,
  Box,
  Modal,
  ModalOverlay,
  ModalHeader,
  ModalBody,
  ModalContent,
  ModalCloseButton,
  useDisclosure,
  FormControl,
  Text
} from "@chakra-ui/react";
import PokemonCard from "@/components/PokemonCard";
import PokemonData from "@/components/PokemonData";
import { useGlobalContext } from "@/context/global";
import PokemonCardSkeleton from "@/components/CardSkeleton";
import NavBar from "@/components/NavBar";
import { formatNumber } from "@/utils/helperFunctions";
import Image from "next/image";

export default function Home() {
  const pokemonDataModal = useDisclosure();
  const { pokemons, nextPage, catchedPokemons, realTimeSearch, loading, loadingPagination, loadingSearch } = useGlobalContext()

  const [selectedPokemon, setSelectedPokemon] = useState();
  const [activeSearch, setActiveSearch] = useState(false);

  function handleViewPokemon(pokemon) {
    setSelectedPokemon(pokemon);
    pokemonDataModal.onOpen();
  }
  //
  pokemons.forEach(pokemon => {
    const found = catchedPokemons.some(pokemonCatched => pokemonCatched.name === pokemon.name);
    pokemon.catched = found;
  });

  //
  const handleSearch = (e) => {
    if (e.target.value.length > 2) {
      realTimeSearch(e.target.value);
      setActiveSearch(true)
    } else if (e.target.value.length === 0) {
      realTimeSearch("");
      setActiveSearch(false)
    }
  };

  return (
    <>
      <NavBar />
      <Flex direction="column" alignItems="center" minH="100vh" justifyContent="start" bg="#f1f1f1">
        <Container maxW="container.xl">
          <Stack p="2" alignItems="center" >
            <form action="" className="search-form" style={{ width: "100%" }}>
              <FormControl className="input-control" onChange={handleSearch}>
                <InputGroup>
                  <Input
                    width="100%"
                    type="text"
                    placeholder="Search for a Pokemon (enter at least 3 characters for search)"
                    aria-label="Search for a Pokemon"
                    borderRadius="full"
                    bg="white"
                    border="2px solid"
                    py="2"
                    borderColor="teal.500"
                    _focus={{ borderColor: "teal.200", boxShadow: "outline" }}
                    textAlign="center"
                  />
                </InputGroup>
              </FormControl>
            </form>
          </Stack>

          <Stack p="5" alignItems="center" spacing="5">
            <SimpleGrid spacing="5" columns={{ base: 1, sm: 2, md: 3, lg: pokemons.length === 0 && !loading ? 1 : 4 }} w="100%">
              {loading || loadingSearch ? (
                // Mientras se cargan los datos, se muestran 20 esqueletos de Card
                Array.from({ length: 20 }).map((_, index) => (
                  <PokemonCardSkeleton key={index} height="300px" width="100%" />
                ))
              ) : pokemons.length === 0 ? (
                // Si no hay ningún Pokémon, muestra la imagen predeterminada
                <Box margin="auto">
                  <Image
                    alt={`not found pokemons`}
                    width={128}
                    height={128}
                    src="/pikachu.png"
                    style={{ margin: "auto" }}
                    loading="lazy"
                  />
                  <Text fontWeight="bold" textAlign="center">Hi there! There is no pokémon that contains those letters</Text>
                </Box>
              ) : (
                // Si los datos se han cargado, mostrar la lista de Pokémon
                pokemons.map((pokemon) => (
                  <Box
                    as="button"
                    key={pokemon.id}
                    onClick={() => handleViewPokemon(pokemon)}
                  >
                    <PokemonCard pokemon={pokemon} />
                  </Box>
                ))
              )}
            </SimpleGrid>
            {!activeSearch &&
              <Button onClick={nextPage} bg="teal.500" color="white" px="5" w="50%" aria-label="Load more Pokemon">
                {loadingPagination ? "Loading..." : "Load More"}
              </Button>}
          </Stack>
        </Container>
      </Flex>

      <Modal {...pokemonDataModal} aria-labelledby="pokemon-modal-header">
        <ModalOverlay />
        <ModalContent>
          <ModalHeader textTransform="capitalize" display="flex" fontWeight="bold" id="pokemon-modal-header">
            {selectedPokemon?.name}  <Text color="#616161" marginLeft="2">N.° {formatNumber(selectedPokemon?.id)}</Text>
          </ModalHeader>
          <ModalCloseButton aria-label="Close Pokemon Details Modal" />
          <ModalBody>
            {selectedPokemon && <PokemonData pokemon={selectedPokemon} />}
          </ModalBody>
        </ModalContent>
      </Modal>
    </>
  );
}
