export const formatNumber = (number) => {
    // Convert the number to a string
    let numberString = number?.toString();
    
    // Check if the number is less than 1000
    if (number < 1000) {
        // Calculate how many leading zeros to add
        let zeros = "0".repeat(4 - numberString.length);
        // Concatenate the zeros and the number
        return zeros + numberString;
    } else {
        // If the number is greater than or equal to 1000, simply return the number string
        return numberString;
    }
}

export const getColorScheme = (typeName) => {
    switch (typeName) {
      case "normal":
        return "#A8A77A";
      case "fighting":
        return "#C22E28";
      case "flying":
        return "#A98FF3";
      case "poison":
        return "#A33EA1";
      case "ground":
        return "#E2BF65";
      case "rock":
        return "#B6A136";
      case "bug":
        return "#A6B91A";
      case "ghost":
        return "#735797";
      case "steel":
        return "#B7B7CE";
      case "fire":
        return "#EE8130";
      case "water":
        return "#6390F0";
      case "grass":
        return "#7AC74C";
      case "electric":
        return "#F7D02C";
      case "psychic":
        return "#F95587";
      case "ice":
        return "#96D9D6";
      case "dragon":
        return "#6F35FC";
      case "dark":
        return "#705746";
      case "fairy":
        return "#D685AD";
      case "unknown":
        return "#68A090";
      case "shadow":
        return "#707070";
      default:
        return "#000000"; 
    }
  };
  