import { formatNumber, getColorScheme } from "@/utils/helperFunctions";
import {
  Stack,
  Text,
  Image,
  HStack,
  Badge,
  AspectRatio,
  Tooltip
} from "@chakra-ui/react";

export default function PokemonCard({ pokemon }) {

  return (
    <Stack
      spacing="5"
      boxShadow="xl"
      p="5"
      w="full"
      borderRadius="sm"
      alignItems="center"
      bg="#ffffff"
    >
      <HStack justifyContent="space-between" w="100%">
        <Text textAlign="center" textTransform="Capitalize" fontWeight="bold" color="#9b9394">
          N.° {formatNumber(pokemon.id)}
        </Text>

        <Tooltip label={pokemon.catched ? "You've caught this Pokémon" : "Catch If You Can"} placement='top' hasArrow>
          <Image
            objectFit="contain"
            src={pokemon.catched ? "pokeball.png" : "pokeball-open.png"}
            alt={pokemon.catched ? "Pokeball (You've caught this Pokémon)" : "Pokeball (Catch If You Can)"}
            maxWidth={{ base: "13%", sm: "15%", md: "17%", lg: "12%" }}
            height="auto"
            aria-label={pokemon.catched ? "You've caught this Pokémon" : "Catch If You Can"}
            role="img"
            loading="lazy"
          />
        </Tooltip>
      </HStack>
      <AspectRatio w="70%" ratio={1}>
        <Image
          src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/home/${pokemon.id}.png`}
          alt={pokemon.name}
          loading="lazy"
        />
      </AspectRatio>
      <Text textAlign="center" textTransform="Capitalize" fontSize="2xl" fontWeight="bold" >
        {pokemon.name}
      </Text>
      <HStack>
        {pokemon.types.map((type) => (
          <Badge size="sm" key={type.slot} px="5" bg={getColorScheme(type.type.name)} color="#f1f1f1">
            {type.type.name}
          </Badge>
        ))}
      </HStack>
    </Stack>
  );
}
