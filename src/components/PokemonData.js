import { formatNumber, getColorScheme } from "@/utils/helperFunctions";
import {
  Box,
  AspectRatio,
  Image,
  Stack,
  SimpleGrid,
  Progress,
  Text,
  Badge,
  HStack,
  Tooltip,
} from "@chakra-ui/react";
import { useState } from "react";
import { useGlobalContext } from "@/context/global";

export default function PokemonData({ pokemon }) {

  const [catched, setCatched] = useState(pokemon.catched);
  const { catchPokemon, releasePokemon } = useGlobalContext();

  const handleCatch = async () => {
    setCatched(!catched);
    if (pokemon.catched) {
      releasePokemon(pokemon.id);
    } else {
      catchPokemon(pokemon);
    }
  };

  return (
    <section aria-label="Pokemon Details">
      <Stack spacing="5" w={{ base: "100%", sm: "75%", md: "100%" }}>
        <Stack spacing="5" position="relative">
          <AspectRatio w={{ base: "100%", sm: "75%", md: "60%" }} ratio={1} margin="auto">
            <Image
              objectFit="contain"
              src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/home/${pokemon.id}.png`}
              alt={pokemon.name}
              loading="lazy"
            />
          </AspectRatio>

          <SimpleGrid columns={{ base: 1, sm: 1, md: 2 }} spacing={5} p={5} bg="gray.100" borderRadius="xl" w="100%">
            <Stack direction="row" spacing="5">
              <Stack>
                <Text fontSize="sm" fontWeight="bold">Weight</Text>
                <Text>{pokemon.weight / 10} kg</Text>
              </Stack>
              <Stack>
                <Text fontSize="sm" fontWeight="bold">Height</Text>
                <Text>{pokemon.height / 10} m</Text>
              </Stack>
              <Stack>
                <Text fontSize="sm" fontWeight="bold">Moves</Text>
                <Text>{pokemon.moves.length}</Text>
              </Stack>
            </Stack>
            <Stack w="100%">
              <Text fontSize="sm" fontWeight="bold">Types</Text>
              <HStack>
                {pokemon.types.map((type) => (
                  <Badge key={type.slot} px="5" bg={getColorScheme(type.type.name)} color="#f1f1f1">
                    {type.type.name}
                  </Badge>
                ))}
              </HStack>
            </Stack>
          </SimpleGrid>
        </Stack>
        <SimpleGrid columns={{ base: 1, sm: 1, md: 2 }} spacing={5} p={5} bg="gray.100" borderRadius="xl" w="100%">
          {pokemon.stats.map((stat) => (
            <div key={stat.stat.name}>
              <Text fontSize="sm" textTransform="Capitalize" fontWeight="bold">{stat.stat.name}</Text>
              <Progress
                bg="gray.300"
                size='md'
                borderRadius="full"
                value={stat.base_stat}
              />
            </div>
          ))}
        </SimpleGrid>
        <Box>
          <Tooltip label={catched ? "Let Pokémon Go" : "Catch this pokémon"} placement='right' hasArrow>
            <Image
              onClick={handleCatch}
              objectFit="contain"
              src={catched ? "pokeball.png" : "pokeball-open.png"}
              alt={catched ? "Pokeball (You've caught this Pokémon)" : "Pokeball (Catch this Pokémon)"}
              maxWidth={{ base: "25%", sm: "25%", md: "20%", lg: "15%" }}
              height="auto"
              margin="auto"
              style={{ cursor: "pointer" }}
              aria-label={catched ? "Let Pokémon Go" : "Catch this pokémon"}
              role="button"
              loading="lazy"
            />
          </Tooltip>
        </Box>
      </Stack>
    </section>
  );
}
