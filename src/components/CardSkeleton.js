import { Stack, Skeleton, AspectRatio } from "@chakra-ui/react";

export default function PokemonCardSkeleton() {
  return (
    <Stack
      spacing="5"
      boxShadow="xl"
      p="5"
      w="full"
      borderRadius="sm"
      alignItems="center"
      bg="#ffffff"
      aria-label="Loading Pokemon Card"
    >
      {/* Title */}
      <Skeleton height="20px" width="40%" aria-label="Loading Pokemon Name" />

      {/* Image */}
      <AspectRatio w="70%" ratio={1} aria-label="Loading Pokemon Image">
        <Skeleton height="100%" width="100%" />
      </AspectRatio>

      {/* Description */}
      <Skeleton height="20px" width="60%" aria-label="Loading Pokemon Description" />

      {/* More Details*/}
      <Stack direction="row" spacing="1" aria-label="Loading Additional Pokemon Details">
        <Skeleton height="20px" width="20%" />
        <Skeleton height="20px" width="20%" />
      </Stack>
    </Stack>
  );
}
