import Head from "next/head";
import {
    Flex,
    Link,
    Image,
    Box,
} from "@chakra-ui/react";

export default function NavBar() {
    return (
        <>
            <Head>
                {/* Metadatos y enlaces */}
                <title>Pokémon Challenge - Alejandro Toledo</title>
                <meta name="description" content="Pokémon Challenge for Abstract" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
            </Head>

            <header>
                <Box bg="teal.500" px={4} py={3}>
                    <Flex alignItems="center">
                        <Link href="/" color="white" fontSize="xl">
                            <Image
                                objectFit="contain"
                                src="pokemon-logo.png"
                                alt="Pokemon Logo"
                                maxWidth={{ base: "55%", sm: "55%", md: "25%", lg: "12%" }}
                                height="auto"
                                margin="auto"
                            />
                        </Link>
                    </Flex>
                </Box>
            </header>
        </>
    )
}
